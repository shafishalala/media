public class Main {
    public static void main(String[] args) {
        Music music = new Music("Hypnogaja - Here Comes The Rain Again", 438);
        music.play();

        Movie movie = new Movie("Fight Club", 150);
        movie.play();
    }
}